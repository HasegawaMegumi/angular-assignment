import { Component, OnInit } from '@angular/core';

// Mapbox GL JSを読み込み
import * as mapboxgl from 'mapbox-gl';

@Component({
  selector: 'app-map-pane',
  templateUrl: './map-pane.component.html',
  styleUrls: ['./map-pane.component.scss']
})
export class MapPaneComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    // マップオブジェクト生成
    // this.mapCreate();
  }

  /**
   * マップオブジェクト生成
   */
   mapCreate() {
    // MIERUNE MONO読み込み
    let map = new mapboxgl.Map({
      container: "map",
      style: {
        "version": 8,
        "sources": {
          "MIERUNEMAP": {
            "type": "raster",
            "tiles": ["https://tile.mierune.co.jp/mierune_mono/{z}/{x}/{y}.png"],
            "tileSize": 256,
            "attribution": "Maptiles by <a href='https://assets-dev.mapfan.com/mapbox/mb-styles/style.std-pc-ja-2.json' target='_blank'>MIERUNE</a>, under CC BY. Data by <a href='http://osm.org/copyright' target='_blank'>OpenStreetMap</a> contributors, under ODbL."
          }
        },
        "layers": [{
          "id": "MIERUNEMAP",
          "type": "raster",
          "source": "MIERUNEMAP",
          "minzoom": 0,
          "maxzoom": 18
        }]
      },
      center: [139.767, 35.681],
      zoom: 11
    });

    // コントロール関係表示
    map.addControl(new mapboxgl.NavigationControl());
   }
}
