import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  template:`
  <mgl-map
    [style]="'https://assets-dev.mapfan.com/mapbox/mb-styles/style.std-pc-ja-2.json'"
    [zoom]="[9]"
    [center]="[-74.50, 40]"
    (load)="map = $event"
  ></mgl-map>
  `
})
export class AppComponent {
  title = 'angular-kadai';
}
